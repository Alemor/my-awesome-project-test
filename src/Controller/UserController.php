<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/user")
    */
    public function new(EntityManagerInterface $entityManager, ValidatorInterface $validator, Request $request): Response
    {
        if (!$_POST) {
            return $this->render('user/new.html.twig');
        }
        $user = new User();
        $user->setFirstName($request->request->get('firstName'));
        $user->setLastName($request->request->get('lastName'));
        $user->setAge($request->request->get('age'));
        $user->setCreatedAt(date_create('now'));
        $errors = $validator->validate($user);
        if (count($errors) > 0) {
            return $this->render('user/new.html.twig', [
                'errors' => $errors,
            ]);
        }
        $entityManager->persist($user);
        $entityManager->flush();
        return $this->render('user/new.html.twig');
    }
}
