<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductController extends AbstractController
{
    /**
     * @Route("/product")
     */
    public function new(EntityManagerInterface $entityManager, Request $request, ValidatorInterface $validator): Response
    {
        $repository = $entityManager->getRepository(User::class);
        $users = $repository->findAll();
        if (!$_POST) {
            return $this->render('product/new.html.twig', [
                'users' => $users,
            ]);
        }
        $product = new Product();
        $product->setTitle($request->request->get('title'));
        $setUser = $repository->find($request->request->get('user'));
        $product->setUser($setUser);
        $product->setCreatedAt(date_create('now'));
        $errors = $validator->validate($product);
        if (count($errors) > 0) {
            return $this->render('product/new.html.twig', [
                'errors' => $errors,
                'users' => $users,
            ]);
        }
        $entityManager->persist($product);
        $entityManager->flush();
        return $this->render('product/new.html.twig', [
            'users' => $users,
        ]);
    }
}
